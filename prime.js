function getPrimeNumbers(min, max){
    if(min > max){
        return [];
    }

    var A = []

    for (var i = 0; i < max+1; i++) {
        A.push(true);
    }

    //Sieve of Eratosthenes
    for(i = 2; i*i < max+1; i++){
        if(A[i]){
            for(var j = 0, k = 0; j < max+1; k++){
                j = i*i+k*i;
                A[j] = false;
            }
        }
    }

    var resultArr = [];

    //write result in array
    for(i = min; i < A.length; i++){
        if(A[i]){
            resultArr.push(i);
        }
    }

    return resultArr;
}

var min = 0;//write your value
var max = 55;//write your value

arr = getPrimeNumbers(min, max);

for (i = 0; i < arr.length; i++) {
    console.log(arr[i]);
}